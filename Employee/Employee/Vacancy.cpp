#include "Vacancy.h"
#include "Header.h"
Vacancy::Vacancy() // �����������
{
	id = -1;
	postName = "";
	workerName = "None";
	isFree = true;
}
Vacancy::Vacancy(int id, string postName, string workerName) // ��� ���� �����������
{
	// ������������� ��� ���������
	this->id = id;
	this->postName = postName;
	this->workerName = workerName;
	if (workerName == "None") // ���� ��� ��������� None, ����� �������� ��������
	{
		isFree = true;
	}
	else // ����� ������
	{
		isFree = false;
	}
}
void Vacancy::printInfo() // ����� ����������
{
	cout << "ID: " << id << setw(8) << " | PostName: " << postName << setw(8) << " | WorkerName: " << workerName << endl;
}
void Vacancy::printInfoIfIsFree() // ����� ����������, ���� �������� �� ������
{
	if (isFree == true)
	{
		cout << "ID: " << id << setw(8) << " | PostName: " << postName << setw(8) << " | WorkerName: None" << endl;
	}
}
string Vacancy::getPostName() // ��������� �������� ���������
{
	return postName;
}
string Vacancy::getWorkerName() // ��������� ����� ���������
{
	return workerName;
}
void Vacancy::setWorkerName(string name) // ���������� ��� ���������
{
	workerName = name;
	isFree = false;
}
void Vacancy::setFree() // ���������� ��������
{
	workerName = "None";
	isFree = true;
}
int Vacancy::getId() // �������� �������������
{
	return id;
}