#include "Header.h"

using namespace std;

Vacancy CreateNewVacancy(int lastId)
{
	string name;
	cout << "Enter postName of new vacancy: ";
	cin >> name;
	Vacancy v(lastId++, name, "None");
	cout << "Done." << endl << endl;
	return v;
}