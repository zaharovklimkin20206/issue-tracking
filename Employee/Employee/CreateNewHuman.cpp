#include "Header.h"

Human CreateNewHuman(int lastId)
{
	string name;
	cout << "Enter name of new worker: ";
	cin >> name;
	Human h(lastId++, name, -1);
	cout << "Done." << endl << endl;
	return h;
}