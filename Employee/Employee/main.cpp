#include "Header.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, ""); //
	vector <Vacancy> vacancies; // ������ ������ ��� ��������
	vector <Human> humans; // ������ ������ ��� �����
	File file;	// ������ ������ ����
	cout << "Retrieving information from a database of vacancies..." << endl;
	vacancies = file.getAllVacancies(); // �������� ��������
	cout << "Count of vacancies: " << vacancies.size() << endl << endl;
	cout << "Retrieving information from a database of humans..." << endl;
	humans = file.getAllHumans(); // �������� �����
	// ��������� �����, ����� ��������� � ������� ������ �������� �� ID
	for (int i = 0; i < humans.size(); i++)
	{
		humans[i].updateVacancy(vacancies);
	}
	cout << "Count of humans: " << humans.size() << endl << endl;

	vector<string> menu = { "List of vacancies", "List of humans", "Create new vacancy", "Create new human", "Manage human", "Save data" };
	int listItem;
	do
	{
		DrawMenu(menu);
		cout << endl << "Enter item number or 0 to exit: ";
		cin >> listItem;
		switch (listItem)
		{
			case 1:
			{
				PrintAllVacancies(vacancies);
				system("pause");
				break;
			}
			case 2:
			{
				PrintAllHumans(humans);
				system("pause");
				break;
			}
			case 3:
				vacancies.push_back(CreateNewVacancy(vacancies.size()));
				break;
			case 4:
				humans.push_back(CreateNewHuman(humans.size()));
				break;
			case 5:
				ManageHuman(humans, vacancies);
				break;
			case 6:
			{
				file.saveHumansData(humans);
				file.saveVacanciesData(vacancies);
				system("pause");
				break;
			}
			system("cls");
		}
	} while (listItem != 0);
	return 0;
}