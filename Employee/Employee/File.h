#pragma once
#include <string>
#include "Human.h"
#include <vector>
#include "Vacancy.h"

using namespace std;

class File
{
private:
	string hPath = "humans.txt"; // ���� ��� �������� �� � ������
	string vPath = "vacancies.txt"; // �� � ����������
public:
	File(); // �����������
	vector<Vacancy> getAllVacancies(); // ��������� ���� ��������
	vector<Human> getAllHumans(); // ���������� ���� �����
	void saveHumansData(vector<Human>); // ��������� �����
	void saveVacanciesData(vector<Vacancy>); // ��������� ��������
};