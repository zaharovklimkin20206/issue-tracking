#pragma once
#include "File.h"
#include "Human.h"
#include "Vacancy.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

void DrawMenu(vector<string>);
void PrintAllVacancies(vector<Vacancy>);
void PrintAllHumans(vector<Human>);
Vacancy CreateNewVacancy(int);
Human CreateNewHuman(int);
void ManageHuman(vector<Human>&, vector<Vacancy>&);