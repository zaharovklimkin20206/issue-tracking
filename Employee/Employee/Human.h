#pragma once
#include <string>
#include "Vacancy.h"
#include <vector>

using namespace std;

class Human
{
private:
	int id;	// �������������
	int vId; // ������������� ��������
	string postName;
	string name; // ��� 
	bool isHired; // ������� �� �� ������
	Vacancy work; // ��������
public:
	Human(); // ������ �����������
	Human(int, string, int); // �����������, ������� ����� ������������
	void setVacancy(Vacancy); // �������� �� ������
	void removeVacancy(); // ������� 
	void updateVacancy(vector<Vacancy>); // �������� ��������
	void printInfo(); // ����� ���� ����������
	void printInfoIfIsNotHired(); // ����� ����������, ���� �� ������ �� ������
	string getName(); // ��������� �����
	int getId(); // �������� �������������
	int getVacancyId(); // �������� ������������� ��������
};