#include "File.h"
#include "Header.h"


File::File() // �����������
{
	ifstream file; // ������ �����
	file.open(hPath, ios::app); // ������� (���� �� ������) 
	file.close(); // ���������
	file.open(vPath, ios::app); // ����������
	file.close();
}

void File::saveHumansData(vector<Human> humans) // ���������� �����
{
	ofstream file(hPath); // ��������� ���� � �� � ������ ������
	for (auto human : humans) // ���������� ������ �����
	{
		file << human.getId() << " " << human.getName() << " " << human.getVacancyId() << endl; // ���������� � ����
	}
	file.close(); // �������� ����
}

void File::saveVacanciesData(vector<Vacancy> vacancies) // ���������� ��������
{
	ofstream file(vPath); // ��������� �� � ������ ������
	for (auto vacancy : vacancies) // ���������� ��� ��������
	{
		file << vacancy.getId() << " " << vacancy.getPostName() << " " << vacancy.getWorkerName() << endl; // ���������� � ����
	}
	file.close(); // ���������
}

vector<Human> File::getAllHumans() // ���������� ������� ����
{
	vector<Human> result;
	ifstream file(hPath);
	while (!file.eof())
	{
		string prefix, name;
		int id, vid;
		file >> id >> name >> vid;
		if (name.length())
		{
			Human that(id, name, vid);
			result.push_back(that);
		}
	}
	return result;
}


vector<Vacancy> File::getAllVacancies() // ��������� ��������
{
	vector<Vacancy> result; // ������� ������ ������
	ifstream file(vPath); // ��������� �� � ������ ������
	while (!file.eof()) // ���� �� ��������� ����� �����
	{
		string post, worker;
		int id;
		file >> id >> post >> worker; // ������
		if (post.length()) // ���� �������� ��������� ����������
		{
			Vacancy that(id, post, worker); // ������� ������ ������ Vacancy � ������� �����������
			result.push_back(that); // ��������� � ������
		}
	}
	return result; // ���������� ������
}