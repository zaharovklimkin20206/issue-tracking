#include "Human.h"
#include "Header.h"

Human::Human() // ������ �����������
{
	id = 0;
	postName = "";
	name = "";
	vId = 0;
	Vacancy empty;
	work = empty;
	isHired = false;
}
Human::Human(int id, string name, int vId) // �����������, ������� ����� ������������
{
	this->id = id;
	this->name = name;
	this->vId = vId;
	isHired = !(vId == -1);
	postName = "";
}
void Human::setVacancy(Vacancy work) // �������� �� ������
{
	this->vId = work.getId();
	this->work = work;
	postName = work.getPostName();
	isHired = true;
}
void Human::removeVacancy() // ������� 
{
	Vacancy empty;
	this->work = empty;
	isHired = false;
	vId = -1;
}
void Human::updateVacancy(vector<Vacancy> vacancies) // �������� ��������
{
	for (auto v : vacancies)
	{
		if (v.getId() == vId)
		{
			work = v;
			postName = v.getPostName();
			break;
		}
	}
}
void Human::printInfo() // ����� ���� ����������
{
	cout << "ID: " << id << setw(4) << " | Name: " << name;
	if (isHired == true)
	{
		cout << setw(4) << " | Vacancy : " << postName << setw(4) << endl;
	}
	else
	{
		cout << endl;
	}
}
void Human::printInfoIfIsNotHired() // ����� ����������, ���� �� ������ �� ������
{
	if (isHired == false)
	{
		cout << "ID: " << id << setw(4) << " | Name: " << name << setw(4) << " | Not hired!" << endl;
	}
}
string Human::getName() // ��������� �����
{
	return name;
}
int Human::getId() // �������� �������������
{
	return id;
}
int Human::getVacancyId() // �������� ������������� ��������
{
	return vId;
}