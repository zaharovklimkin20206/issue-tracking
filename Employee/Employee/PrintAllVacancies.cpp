#include "Header.h"

using namespace std;
void PrintAllVacancies(vector<Vacancy> vacancies)
{
	cout << "=== [All vacancies] ===" << endl;
	for (auto v : vacancies)
	{
		v.printInfo();
	}
	cout << endl << endl << "=== [Free only] ===" << endl;
	for (auto v : vacancies)
	{
		v.printInfoIfIsFree();
	}
	cout << endl;
}