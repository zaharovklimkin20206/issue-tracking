#include "Header.h"
using namespace std;

void ManageHuman(vector<Human>& humans, vector<Vacancy>& vacancies)
{
	PrintAllHumans(humans);
	cout << endl;
	cout << "Enter human ID: ";
	int id;
	cin >> id;
	if (id < humans.size())
	{
		cout << "Manage " << humans[id].getName() << endl;
		vector<string> menu = { "Hire", "Unhire", "Info" };
		int key;
		do
		{
			DrawMenu(menu);
			cout << endl << "Enter key or 0 to back: ";
			cin >> key;
			switch (key)
			{
			case 1:
			{
				PrintAllVacancies(vacancies);
				cout << endl << "Enter vacancy id: ";
				int vId = -1;
				cin >> vId;
				if (vId < vacancies.size())
				{
					humans[id].setVacancy(vacancies[vId]);
					vacancies[vId].setWorkerName(humans[id].getName());
					cout << "Done!";
					system("pause");
				}
				else
				{
					cout << "Incorrect id.";
				}
				break;
			}
			case 2:
			{
				vacancies[humans[id].getVacancyId()].setFree();
				humans[id].removeVacancy();
				break;
			}
			case 3:
			{

				humans[id].printInfo();
				break;
			}
			}
		} while (key != 0);
	}
}