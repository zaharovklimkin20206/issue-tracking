#include "Header.h"

using namespace std;

void PrintAllHumans(vector<Human> humans)
{
	cout << "=== [All humans] ===" << endl;
	for (auto h : humans)
	{
		h.printInfo();
	}
	cout << endl << endl << "=== [Free only] ===" << endl;
	for (auto h : humans)
	{
		h.printInfoIfIsNotHired();
	}
	cout << endl;
}