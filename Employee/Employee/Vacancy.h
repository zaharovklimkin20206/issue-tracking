#pragma once
#include <string>

using namespace std;

class Vacancy
{
private:
	int id; // �������������
	string postName; // �������� ���������
	bool isFree; // �������� ��
	string workerName; // ��� ���������
public:
	Vacancy(); // �����������
	Vacancy(int, string, string); // ��� ���� �����������
	void printInfo(); // ����� ����������
	void printInfoIfIsFree(); // ����� ����������, ���� �������� �� ������
	string getPostName(); // ��������� �������� ���������
	string getWorkerName(); // ��������� ����� ���������
	void setWorkerName(string); // ���������� ��� ���������
	void setFree(); // ���������� ��������
	int getId(); // �������� �������������
};

